/**
 * This class is used by the SongCollection program to store Song objects that will be added and manipulated through
 * the SongCollection class. Each album can contain 4 songs up to a duration of 720 seconds (12 minutes).
 *
 * @Author: Lachlan Court
 * @Modified 11/06/2020
 * @Version 1.0
 * @StudentNumber: c3308061
 */

public class Album
{
    // instance variables 
    private String name;
    //The number of songs can be freely adjusted from changing this array declaration
    private Song[] songs = new Song[5];
    //The logical size of the songs array
    private int songsSize = 0;
    //Keep track of the current total time of the album
    private int totalTime;
    //The total duration of songs on the album cannot exceed 12 minutes. 12*60=720
    private final int MAX_TIME = 720;

    /*
      Constructor method takes a string for the name of the album and sets the totalTime counter to 0
    */
    public Album(String _name)
    {
        name = _name;
        totalTime = 0;
    }

    //--------------------- Getters ---------------------
    public String getName()
    {
        return name;
    }

    public int getTotalTime()
    {
        return totalTime;
    }

    public int getMaxTime()
    {
        return MAX_TIME;
    }

    public int getSongsSize()
    {
        return songsSize;
    }

    public String getSongName(int songNumber)
    {
        /*
        This method takes a song number and returns the name of that song. This method is not called unless it is known
        that the song exists, so there is no reason to check.
         */
        return songs[songNumber].getName();
    }

    //--------------------- Checking methods ---------------------
    public boolean isFull()
    {
        /*
        This method checks the current object to see if the logical size matches the physical size of the array
        */
        return (songsSize == songs.length);
    }

    public boolean hasDuplicate(Song newSong)
    {
        /*
        This method takes a song object as an argument and checks it against the other songs in the album object to see
        if it already exists.
         */

        //Loop through the songs array
        for (int i = 0; i < songsSize; i++)
        {
            //For each song, check it against the song in the argument using the isSameSong method
            if (isSameSong(songs[i], newSong))
            {
                return true;
            }
        }
        return false;
    }

    //--------------------- Menu Methods ---------------------
    public void addSong(Song newSong)
    {
        /*
        This method takes a new song object and adds it into this album object.

        At this point it has already been checked that there is a free spot on the album, so there is no need to check
        again. Add the song into the songs array and then increment the logical size of the array
         */
        songs[songsSize] = newSong;
        songsSize++;
        /*
        The totalTime has also already been checked, so no need to check again. Update the total time to ensure that
        songs that are too big to fit on the album cannot be added at the same time as this new one
         */
        totalTime += newSong.getIntDuration();
    }

    public String showSongs(boolean showDetails, boolean saving)
    {
        /*
        This method is called from a number of areas in the program and as such takes two boolean arguments referring to
        what kind of data should be returned. The method can return either a string of just song names, in which case
        the showDetails argument would be false, or a string of song names with all the song details, in which case
        showDetails would be true. The saving argument will be true if the String should be unformatted plain text. If
        it is false then the String will also be formatted so that the songs will be presented neatly and indented
         */

        //This will keep track of all the details as they are added, and will be returned in one go at the end
        String songDetails = "";

        /*
        First add an opening statement, but only if the method will be returning all the song details. If it is
        returning only song names, they will be under the name of the album, and as such the following message will be
        implied
         */
        if (showDetails)
        {
            songDetails = "The following songs are on this album\n\n";
        }

        //Loop through the songs array and add the details for each song, simply forwarding on the arguments
        for (int i = 0; i < songsSize; i++)
        {
            songDetails += getSongDetails(i, showDetails, saving);
        }

        /*
        If the logical size of the array is 0 then there are no songs. Depending on the value of showDetails this may
        require additional formatting
         */
        if (songsSize == 0)
        {
            if (!showDetails)
            {
                songDetails = "    There are no songs on this album.\n";
            }
            else
            {
                songDetails = "There are no songs on this album.";
            }
        }

        //Finally return the songDetails string
        return songDetails;
    }

    public String getSongsUnderDuration(int specifiedDuration)
    {
        /*
        This method receives a song duration as an integer of seconds and returns a formatted String of all the songs
        that have a duration less than the one specified
         */

        //This will be a running String of all the songs under the specified duration, to be returned at the end
        String songsUnderDuration = "";

        /*
        Loop through the songs array and then check if the duration is under the specified duration. If so, add the
        song name to songsUnderDuration. Add newline characters for formatting purposes
         */
        for (int i = 0; i < songsSize; i++)
        {
            if (songs[i].getIntDuration() < specifiedDuration)
            {
                songsUnderDuration += songs[i].getName() + "\n";
            }
        }

        //Finally return the result to the original method
        return songsUnderDuration;
    }

    public String getSongsOfGenre(String specifiedGenre)
    {
        /*
        This method receives a genre of songs and returns all the songs that are classified as that genre as a
        formatted String
         */

        //This will be a running String of all the songs of the specified genre, to be returned at the end
        String songsOfGenre = "";

        /*
        Loop through the songs array and then check if the genre is the same as the one specified. If so, add the song
        name to songsOfGenre. Add newline characters for formatting purposes
         */
        for (int i = 0; i < songsSize; i++)
        {
            if (songs[i].getGenre().compareTo(specifiedGenre) == 0)
            {
                songsOfGenre += songs[i].getName() + "\n";
            }
        }

        //Finally return the result to the original method
        return songsOfGenre;
    }

    public void deleteSong(int songNumber)
    {
        /*
        This method takes a song number as an integer and deletes that song.

        Subtract the length of the song from the totalTime of the album to make space for potential new songs
        */
        totalTime -= songs[songNumber].getIntDuration();

        //Loop through the songs array from where the song to be deleted is, to the end. Shuffle each song left once
        for (int i = songNumber; i < songsSize - 1; i++)
        {
            songs[i] = songs[i + 1];
        }
        /*
        Decrement the logical size, effectively hiding the song that is at the end of the array at the moment, and thus
        effectively deleting it
         */
        songsSize--;
    }

    public String getSongDetails(int songNumber, boolean showDetails, boolean saving)
    {
        /*
        This method takes an index in the songs array and two boolean arguments to control the nature of the song
        details returned
         */

        //The details will be added to this String to be added at the end
        String details = "";

        //Depending on the value of the boolean arguments, add some initial information
        if (!showDetails)
        {
            details += "    ";
        }
        if (saving)
        {
            details += "Name ";
        }
        //Add the name of the song
        details += songs[songNumber].getName();

        //Only add the details if showDetails is true
        if (showDetails)
        {
            /*
            Add the details of the songs, as well as some blank space at the start and end of each line of details
            to indent a little bit for neat formatting

            Adjust the formatting depending on the value of the saving argument
             */
            String colon = ":";
            String spaces = "    ";
            if (saving)
            {
                colon = "";
                spaces = "";
            }

            //Add the details of the song
            details += "\n" + spaces + "Artist" + colon + " " + songs[songNumber].getArtist() + "\n" + spaces;
            details += "Genre" + colon + " " + songs[songNumber].getGenre() + "\n" + spaces;
            details += "Duration" + colon + " " + songs[songNumber].getStringDuration() + "\n\n";
        }
        else
        {
            //If showDetails is false, just add a newline character
            details += "\n";
        }

        //Finally return the String
        return details;
    }

    public void sortSongs()
    {
        //Create a temporary song for the purposes of swapping Song objects around in the songs array
        Song tempSong;
        //Loop through twice to sort one song for each iteration of the i loop
        for (int i = 0; i < songsSize; i++)
        {
            for (int j = 0; j < songsSize - 1; j++)
            {
                /*
                Determine the length of the smallest name out of the two being compared to avoid an index out of bounds
                exception
                 */
                int smallestName = songs[j].getName().length();
                if (smallestName > songs[j + 1].getName().length())
                {
                    smallestName = songs[j + 1].getName().length();
                }

                //Loop through each character of the song names and compare them against each other using ASCII values
                for (int k = 0; k < smallestName; k++)
                {
                    //If the character is the same, ignore it
                    if ((int) songs[j].getName().toLowerCase().charAt(k) == (int) songs[j + 1].getName().toLowerCase().charAt(k))
                    {
                        continue;
                    }
                    /*
                    If the character of the first song comes after the character of the second song alphabetically then
                    they should be swapped
                     */
                    else if ((int) songs[j].getName().toLowerCase().charAt(k) > (int) songs[j + 1].getName().toLowerCase().charAt(k))
                    {
                        tempSong = songs[j];
                        songs[j] = songs[j + 1];
                        songs[j + 1] = tempSong;
                        //Once a swap has been made there's no need to check the rest of the word
                        break;
                    }
                    else
                    {
                        //If the first album comes before the second album alphabetically, there's no need for a swap
                        break;
                    }
                }
            }
        }
    }
    //--------------------- Private Methods ------------------------------
    private boolean isSameSong(Song a, Song b)
    {
        /*
        This method takes two songs and compares them to see if they are the same or not. It is only used by other
        methods within the Album class therefore the method is private.

        Critera for a duplicate song are name, artist and duration need to be the same. Check each of these and return
        true if they all match or false if any of them are different
         */
        return ((a.getName().toLowerCase().compareTo(b.getName().toLowerCase()) == 0) && (a.getArtist().toLowerCase().compareTo(b.getArtist().toLowerCase()) == 0) && (a.getIntDuration() == b.getIntDuration()));
    }
}