/**
 * This class stores information about individual songs that are inside the Album class object
 *
 * @Author: Lachlan Court
 * @Modified 04/05/2020
 * @Version 1.0
 * @StudentNumber: c3308061
 */

public class Song
{
    private String name;
    private String artist;
    private int duration;
    private String genre;

    public Song(String _name, String _artist, String _genre, int _duration)
    {
        name = _name;
        artist = _artist;
        duration = _duration;
        genre = _genre;
    }

    //--------------------- Getters ---------------------
    public String getName()
    {
        return name;
    }
    public String getArtist()
    {
        return artist;
    }
    public String getGenre()
    {
        return genre;
    }
    public int getIntDuration()
    {
        return duration;
    }
    public String getStringDuration()
    {
        /*
        The duration is stored as an integer of seconds, although some areas of the programme require it to be in a
        String formmated as mm:ss
         */
        return convertDurationToString(duration);
    }

    //--------------------- Private Methods ---------------------
    private String convertDurationToString(int secsInput)
    {
        /*
        This method takes a duration in seconds, and converts it to the format mm:ss
        Used when showing song details as the duration is stored in seconds but should be
        displayed to the user as mm:ss

        Start by declaring the variables for the minute portion and the second portion of the duration
         */
        String minPortion;
        String secPortion;

        //Divide the duration in seconds by 60 and convert to an integer to remove the decimal
        int minutes = (int)secsInput / 60;

        //Convert the new minutes value to a string and store in minPortion
        minPortion = String.valueOf(minutes);

        //Calculate the remainder and convert it to a string to be stored in secsPortion
        secPortion = String.valueOf(secsInput - (minutes * 60));

        //If the seconds portion is a single digit number, add a 0 in front of the digit
        if (secPortion.length() == 1)
        {
            secPortion = "0" + secPortion;
        }

        //Return the duration in the format mm:ss
        return minPortion + ":" + secPortion;
    }
}