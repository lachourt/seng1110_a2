/**
 * This programme acts as a database for a number of music albums. It shows a menu up to the user and allows them to
 * create albums and add songs, delete albums and songs, and also has functionality to display data about these
 * songs and albums once they have been created
 *
 * @Author: Lachlan Court
 * @Modified 11/06/2020
 * @Version 1.0
 * @StudentNumber: c3308061
 */

//Allows GUI functionality for input/output
import javax.swing.*;
//Allows scanning of a directory to get all available txt files to read as input
import java.nio.file.*;
//Allows use of Scanner class to read txt files
import java.util.*;
//Allows opening of files for reading
import java.io.*;

public class SongCollection
{
    //The number of albums can be freely adjusted from changing this array declaration
    private Album[] albums = new Album[4];
    private int albumsSize = 0;
    //The menu item the user selects will be stored here
    private String menuUserInput;
    //The menu input will be converted to an Integer so that it can be checked using the validMenuEntry method
    private int action = -1;
    /*
    The name for the collection to be saved as. If the user loads a file then this will be overwritten as the name of
    the original file. If they start with a new collection they will select the name before saving
     */
    private String collectionName = "";
    //If the user has not added any new albums or songs since sorting, they will not be asked to sort it again
    private boolean sortedCollection = false;

    public void run()
    {
        /*
        Start by asking the user if they wish to read from a text file. If not, nothing gets added and the user starts
        with an empty collection. If yes the file will be opened and interpretted to initialise with the correct data
         */
        readFromTextFile();

        //The main loop will continue to run until quit = true which is determined in the quitConfirmation_Menu method
        boolean quit = false;
        while (!quit)
        {
            //Reset menuUserInput variable after the last action
            menuUserInput = "-";
            do
            {
                //Show the user the menu, and ask for the next action to be performed
                menuUserInput = input("1: Create album\n2: Add song\n3: Request all songs with a specific name\n4: Show song details from an album\n5: Show a list of albums with their songs\n6: Show a list of all songs under a specific duration\n7: Show a list of songs of a specific genre\n8: Delete an album\n9: Delete a song from an album\n0: Exit\nWhat do you wish to do?");
            }
            /*
            Ensure that the user has entered a valid number 1-9 or 0. If they have not, the validMenuEntry method will
            loop and ask the user again until they do
             */
            while (!validMenuEntry(menuUserInput, 0, 9));

            //Convert the user input to an integer
            action = Integer.parseInt(menuUserInput);

            //A switch case will determine which menu item the user selected, and call the appropriate method
            switch (action)
            {
                case 1:
                {
                    createAlbum_Menu();
                    break;
                }
                case 2:
                {
                    addSong_Menu();
                    break;
                }
                case 3:
                {
                    showAllSongsOfName_Menu();
                    break;
                }
                case 4:
                {
                    showSongDetails_Menu();
                    break;
                }
                case 5:
                {
                    showAlbums_Menu();
                    break;
                }
                case 6:
                {
                    showSongsUnderDuration_Menu();
                    break;
                }
                case 7:
                {
                    showSongsOfSpecificGenre_Menu();
                    break;
                }
                case 8:
                {
                    deleteAlbum_Menu();
                    break;
                }
                case 9:
                {
                    deleteSong_Menu();
                    break;
                }
                case 0:
                {
                    quit = quitConfirmation_Menu();
                    break;
                }
            }
        }
        //After the while loop has ended, produce a final goodbye message before the program terminates
        output("See you soon!");
    }

    public static void main(String[] args)
    {
        SongCollection sg = new SongCollection();
        sg.run();
    }

    //--------------------- Menu functions ---------------------
    private void createAlbum_Menu()
    {
        /*
        This method allows the user to create an album in the SongCollection database
         */

        //Declare the variable to store the name of the album that the user enters
        String albumName;

        /*
        Check to ensure there is available space in the albums array. If the logical size is the same as the physical
        size, there is no space and the user is notified and returned to the main menu
         */
        if (albumsSize == albums.length)
        {
            output("There are no free albums available at the moment. Please delete an album before adding another");
            return;
        }

        //If there is a free album, ask the user for it's name
        albumName = input("What is the name of the album?");

        /*
        Next, ensure that the album is not a duplicate. The albumExists method loops through the album array and if the
        user-specified name is already in the array, notify the user and return to the main menu
         */
        if (albumExists(albumName))
        {
            output("That album already exists!");
            return;
        }

        /*
        There is now an available album and the name entered is not the name of an existing album. The creation of the
        album is done using the makeAlbum method
         */
        makeAlbum(albumName);

        /*
        With a new album added in, next time the user displays albums or songs they will be asked if they wish to sort
        alphabetically
         */
        sortedCollection = false;

        //And finally show a success message
        output("Album \"" + albumName + "\" created successfully.");
    }

    private void addSong_Menu()
    {
        /*
        This method asks the user to enter a song name and shows the user if songs of the same name already exist. If
        they wish to continue, the user will then be asked to pick an album and specify the artist, genre and
        duration of the song to be added to the album
         */

        //The following variables will store information about the song as the user enters it
        String songName;
        String songArtist;
        String songGenre = "";
        String songDurationInput;
        //The duration is stored as an integer number of seconds and will be converted and stored in this variable
        int songDuration;
        //The number of the album that the user wants to add add a song to
        int addSongAlbumSelection = 0;

        /*
        If the logical size of the array is 0, there are no albums to add songs to. Notify the user and return to the
        main menu
         */
        if (albumsSize == 0)
        {
            output("You haven't created any albums! Please create some albums first.");
            return;
        }

        songName = input("What is the name of the song?");
        /*
        The shouldAddNewSongWithSameName method will present the user with all (if any) songs that have the same name
        as the one they just entered. They will be asked if they wish to continue, and the method returns a boolean
        true if they wish to continue, false if they do not.
         */
        if (!shouldAddNewSongWithSameName(songName))
        {
            //If the user has made a mistake and does not want to add the same song again, return to main menu
            return;
        }

        /*
        Now that the song name has been specified, determine which album the song should be added to. This is done
        using the getAlbumSelection method. This puts together a list of albums and asks the user to select one. It
        returns an integer number referring to the index in the albyms array, or -1 return to main menu.
         */
        addSongAlbumSelection = getAlbumSelection("Select which album to add the song to");
        if (addSongAlbumSelection == -1)
        {
            //If the user has selected return to main menu the the variable addSongAlbumSelection will be == -1
            return;
        }

        // If the album is full, notify the user and then return to the main menu
        if (albums[addSongAlbumSelection].isFull())
        {
            output("There are no free tracks on that album at the moment. Please delete a song before adding another");
            return;
        }

        //Having confirmed that there is space on the selected album, start gathering info about the song
        songArtist = input("What is the artist for the song?");

        /*
        Ask the user to enter a genre using the getGenreSelection method. This method is used in a number of other
        areas of the program and as such has an argument for the prompting message as it will be different depending on
        which area of the program it is called from
         */
        songGenre = getGenreSelection("What is the genre of the song?");

        //Ask the user for the duration of the song
        do
        {
            songDurationInput = input("Enter the duration of the song either in seconds or in the format \"mm:ss\"");
        }
        /*
        The user can specify the duration as either an integer number of seconds or in the format mm:ss. As such check
        for both. The method convertDurationToSecs will return a -1 for any invalid input whether that is not an
        integer, or a misformatted mm:ss entry. A valid entry will return the duration in seconds as an integer number.
        In both formats the duration can also not be 0. As such check that the duration is >= 1.
         */
        while (!((convertDurationToSecs(songDurationInput) >= 1) || ((isInteger(songDurationInput)) && (Integer.parseInt(songDurationInput) >= 1))));

        /*
        With a valid duration entered, convert it to seconds to be stored in the Song object. First assume that it is
        in the format mm:ss
         */
        songDuration = convertDurationToSecs(songDurationInput);
        //If songDuration is now -1, the duration must have been an integer number of seconds. Parse is straight in
        if (songDuration == -1)
        {
            songDuration = Integer.parseInt(songDurationInput);
        }

        /*
        The makeSong method produces an exitCode depending on why it failed. This process is in place due to the fact
        that the makeSong method is also used during file reading and should not be showing up messages when used in
        that way
         */
        int exitCode = makeSong(songName, songArtist, songGenre, songDuration, albums[addSongAlbumSelection]);

        /*
        Interpret the exitCode receieved and if necessary, notify the user and return to the main menu. A successful
        run will produce an exit code of 0
         */
        if (exitCode == 1)
        {
            output("That song already exists!");
            return;
        }
        else if (exitCode == 2)
        {
            output("That song goes for too long! It will not fit on the album.");
            return;
        }

        /*
        With a new song added in, next time the user displays albums or songs they will be asked if they wish to sort
        alphabetically
         */
        sortedCollection = false;

        //And finally show a success message
        output("Song \"" + songName + "\" added to album \"" + albums[addSongAlbumSelection].getName() + "\" successfully");
    }

    private void showAllSongsOfName_Menu()
    {
        /*
        This method asks the user to enter the name of a song, and then displays all the song details of the songs that
        share a name with the one the user specifies
         */

        //If the logical size is 0, the user hasn't created any albums. Notify the user and return to the main menu
        if (albumsSize == 0)
        {
            output("You haven't created any albums! Please create some albums first.");
            return;
        }

        //Ask the user for the name of the song they want to check
        String songName = input("Please enter the name of the song");
        //Start the output for the user with the following statement which will be added to as information is gathered
        String userOutput = "The list of songs in this collection that have the specified name is as follows\n\n";
        //Store all the song information in this string to be added to the userOutput at the end
        String songList;
        /*
        The method getSongsOfSameName takes a song name and shows all the song details of songs that share names with
        the one the user specifies
         */
        songList = getSongsOfSameName(songName, false);
        /*
        The method getSongsOfSameName will return "-1" if there are no songs that math the one the user specifies. If
        so, replace the whole message with a message for the user
         */
        if (songList.compareTo("-1") == 0)
        {
            userOutput = "There are no songs in this collection that match the name specified";
        }
        //If the returned string is anything other than "-1", add the string onto the userOutput String
        else
        {
            userOutput += songList;
        }

        //And finally show the output to the user
        output(userOutput);
    }

    private void showSongDetails_Menu()
    {
        /*
        This method asks the user to pick an album and then shows the songs and details of those songs that are in
        the album that was selected
         */

        //If the logical size is 0, the user hasn't created any albums. Notify the user and return to the main menu
        if (albumsSize == 0)
        {
            output("You haven't created any albums! Please create some albums first.");
            return;
        }

        sortAlphabetically();

        //The number of the album that the user wants to add add a song to
        int addSongAlbumSelection;

        /*
        First call the getAlbumSelection method. This puts together a list of albums and asks the user to select one.
        It returns an integer number referring to the index in the albums array or -1 return to main menu
         */
        addSongAlbumSelection = getAlbumSelection("Select which album to show details for");

        //If the user has selected return to main menu the the variable addSongAlbumSelection will be == -1
        if (addSongAlbumSelection == -1)
        {
            return;
        }

        /*
        The showSongs method is used in a number of areas of the program and as such requires two boolean arguments to
        control the kind of output received. "true" in the first argument will return all details of the songs and
        "false" will only return the song names. The value of the second argument purely changes the formatting, "true"
        will have no spaces and is used when saving text files. For most methods it will be "false" and will have
        spaces and indents.
         */
        output(albums[addSongAlbumSelection].showSongs(true, false));
    }

    private void showAlbums_Menu()
    {
        /*
        This method shows all albums that have been created, as well as all songs on those albums
         */

        //If the logical size is 0, the user hasn't created any albums. Notify the user and return to the main menu
        if (albumsSize == 0)
        {
            output("You haven't created any albums! Please create some albums first.");
            return;
        }

        sortAlphabetically();

        //This will hold all the details as they are added, and will be displayed in one go at the end
        String albumDetails = "The albums you have in this collection are:\n\n";

        //Check each album in a for loop and add the details to albumDetails
        for (int i = 0; i < albumsSize; i++)
        {
            //Add the name of the album first, and the names of the songs will be added later
            albumDetails += albums[i].getName() + "\n";
            /*
            The showSongs method is used in a number of areas of the program and as such requires two boolean arguments to
            control the kind of output received. "false" in the first argument will only return the song names and
            "true" will return all details of the songs. The value of the second argument purely changes the formatting,
            "true" will have no spaces and is used when saving text files. For most methods it will be "false" and will
            have spaces and indents.
            */
            albumDetails += albums[i].showSongs(false, false) + "\n";
        }

        //Finally output the contents of the albumDetails variable to the user
        output(albumDetails);
    }

    private void showSongsUnderDuration_Menu()
    {
        /*
        This method asks the user to specify a duration and then lists all songs in the SongCollection database that
        have a duration under that specific length
         */

        //If the logical size is 0, the user hasn't created any albums. Notify the user and return to the main menu
        if (albumsSize == 0)
        {
            output("You haven't created any albums! Please create some albums first.");
            return;
        }

        //This will hold the initial input from the user until a positive integer number has been entered.
        String specifiedDurationInput;
        /*
        The user specified duration will be converted to an integer number of seconds. Every song under this duration
        will be added to the output
         */
        int specifiedDuration;
        //This will keep track of all songs under the specified duration and will be displayed at the end of the method
        String songsUnderDuration = "";

        //Next, ask the user for the duration that they want to compare to all of the songs in the collection
        do
        {
            specifiedDurationInput = input("Please enter a positive number that is the maximum song duration either in seconds or in the format \"mm:ss\"");
        }
        /*
        The user can specify the duration as either an integer number of seconds or in the format mm:ss. As such check
        for both. The method convertDurationToSecs will return a -1 for any invalid input whether that is not an
        integer, or a misformatted mm:ss entry. A valid entry will return the duration in seconds as an integer number.
        In both formats the duration can also not be 0. As such check that the duration is >= 1.
         */
        while (!((convertDurationToSecs(specifiedDurationInput) >= 1) || ((isInteger(specifiedDurationInput)) && (Integer.parseInt(specifiedDurationInput) >= 1))));

        /*
        With a valid duration entered, convert it to seconds to be stored in the Song object. First assume that it is
        in the format mm:ss
         */
        specifiedDuration = convertDurationToSecs(specifiedDurationInput);
        if (specifiedDuration == -1)
        {
            //If songDuration is now -1, the duration must have been an integer number of seconds. Parse is straight in
            specifiedDuration = Integer.parseInt(specifiedDurationInput);
        }

        /*
        Loop through the albums array and call the Album Class's getSongsUnderDuration method to receieve a String of
        all songs under that duration. Add these to the songsUnderDuration String
         */
        for (int i = 0; i < albumsSize; i++)
        {
            songsUnderDuration += albums[i].getSongsUnderDuration(specifiedDuration);
        }

        //Now all we need to do is show the result to the user
        if (songsUnderDuration.length() != 0)
        {
            /*
            If songsUnderDuration is not empty, at least one song exists that has a duration under one specified by the
            user. Show this output to the user
             */
            output("The songs in this collection that have a duration under " + specifiedDurationInput + " are as follows:\n\n" + songsUnderDuration);
        }
        else
        {
            //If songsUnderDuration is empty, there are no songs that are under the specified duration
            output("There are no songs in this collection under the specified duration.");
        }
    }

    private void showSongsOfSpecificGenre_Menu()
    {
        /*
        This method asks the user to enter a genre and then displays all songs that are classified as that genre
         */

        //If the logical size is 0, the user hasn't created any albums. Notify the user and return to the main menu
        if (albumsSize == 0)
        {
            output("You haven't created any albums! Please create some albums first.");
            return;
        }

        //Declare the variable to hold the genre the user specifies
        String specifiedGenre;
        //A running string of all songs of the specified genre which will be displayed at the end of the method
        String songsOfGenre = "";

        //Next, ask the user to enter a genre that they want to compare to all of the songs in the collection
        specifiedGenre = getGenreSelection("Please enter a genre");

        /*
        Loop through the albums array and call the Album Class's getSongsOfGenre method. This returns a String of all
        songs with the specified genre
         */
        for (int i = 0; i < albumsSize; i++)
        {
            songsOfGenre += albums[i].getSongsOfGenre(specifiedGenre);
        }

        //Now all we need to do is show the result to the user
        if (songsOfGenre.length() != 0)
        {
            /*
            If songsOfGenre is not empty, at least one song exists that has the genre specified by the user. Show this
            output to the user
             */
            output("The songs in this collection that are classified as " + specifiedGenre + " are as follows:\n\n" + songsOfGenre);
        }
        else
        {
            //If songsOfGenre is empty, there are no songs that are of the specified genre
            output("There are no songs in this collection of the specified genre.");
        }
    }

    private void deleteAlbum_Menu()
    {
        /*
        This method shows a list of all albums that exist and asks the user to pick one to be deleted
         */

        //If the logical size is 0, the user hasn't created any albums. Notify the user and return to the main menu
        if (albumsSize == 0)
        {
            output("There are no albums to delete");
            return;
        }

        //Declare the variable for the album that the user wants to delete. An integer number 1, 2, 3
        int deleteAlbumSelection;
        //The name of the album the user wants to delete will be pulled out for a confirmation message
        String deletedAlbumName;

        /*First call the getAlbumSelection method. This puts together a list of existing albums and asks the user to
        select one. It returns an integer number referring to the album array index or -1 return to main menu
         */
        deleteAlbumSelection = getAlbumSelection("Select which album to delete:");
        if (deleteAlbumSelection == -1)
            return;

        //Pull out the name of the album in order to have a confirmation message
        deletedAlbumName = albums[deleteAlbumSelection].getName();

        /*
         Make sure the user is sure they want to delete the album. They must explicitly type the album name, otherwise
         the method will give the user an error message and return to main menu as a failed attempt
         */
        if (input("Please type \"" + deletedAlbumName + "\" below to confirm deletion").compareTo(deletedAlbumName) != 0)
        {
            output("Album deletion failed");
            return;
        }

        /*
        The album will be deleted by shuffling the albums that are later in the array forwards one place and then
        decrementing the logical size of the array
        */
        for (int i = deleteAlbumSelection; i < albumsSize - 1; i++)
        {
            albums[i] = albums[i + 1];
        }
        albumsSize--;

        //Show confirmation message
        output("Album \"" + deletedAlbumName + "\" deleted succesfully!");
    }

    private void deleteSong_Menu()
    {
        /*
        This method shows a list of songs in alphabetical order and asks the user to select one to be deleted
         */

        /*
        An array of length 2 will be returned by the method getSongToDelete(). The first item will refer to the index
        in the albums array and the second item will be the the index in the songs array of that album
         */
        int[] albumSongCode;

        /*
        The method getSongToDelete() presents the user with a menu of all existing songs in alphabetical order, and
        asks the user to select one. It will return an array referring to the song selected
         */
        albumSongCode = getSongToDelete();

        //If the first item in the albumSongCode array is -1, The user wishes to return to main menu
        if (albumSongCode[0] == -1)
        {
            return;
        }

        /*
        With the songNumber and the tempAlbum we can now pull the name of the song that the user wishes to delete out
        and store it the String songName where it can be shown to the user for confirmation
         */
        String songName = albums[albumSongCode[0]].getSongName(albumSongCode[1]);

        /*
        The user will be presented with the song that they selected and all the details of that song. Ask the user to
        confirm that they have selected the correct song by having them enter the name of the song. If they mistype,
        the song will not be deleted. They must enter it explicitly.
         */
        if (input("The song you have selected to delete is as follows: \n\n" + albums[albumSongCode[0]].getSongDetails(albumSongCode[1], true, false) + "Please type \"" + songName + "\" below to confirm deletion").compareTo(songName) != 0)
        {
            //If they have not typed the song name correctly, notify the user and return to the main menu
            output("Song deletion failed");
            return;
        }

        /*
        With the confirmation successful, call the Album Class's method deleteSong and pass in the integer for which
        song to delete
         */
        albums[albumSongCode[0]].deleteSong(albumSongCode[1]);

        //Show a success message
        output("Song deleted successfully");
    }

    private boolean quitConfirmation_Menu()
    {
        /*
        This method ensures that the user wishes to quit and also asks if the user wishes to save the collection
         */

        //Confirm that the user intended to quit
        boolean response = confirm("Are you sure you wish to quit?");
        //If the user wishes to quit, ask if they wish to save
        if ((response) && (confirm("Do you wish to save?")))
        {
            /*
            If the user wishes to save, try saving. If the method saveTextFile returns false, there was an error in
            saving
             */
            if (!saveTextFile())
            {
                //Because there was an error saving, ensure the user still wishes to still quit. If no, return false
                if (!confirm("Error saving collection. Do you wish to exit without saving?"))
                {
                    return false;
                }
            }
            output("File saved successfully as " + collectionName + "!");
        }
        //Return the response that the user selected
        return response;
    }

    private void sortAlphabetically()
    {
        /*
        This method will ask the user if they wish to sort the collection alphabetically, and if the user clicks yes
        then it will do so
         */

        /*
        Only run this method if there has been changes to the collection. If it has already been sorted there's no
        reason to sort it again
         */
        if ((sortedCollection) || (!confirm("Do you wish to sort the collection alphabetically?")))
        {
            return;
        }

        //Once the album has been sorted, the user will not be asked again until they add new albums or songs
        sortedCollection = true;

        //Create a temporary album for the purposes of swapping Album objects around in the albums array
        Album tempAlbum;
        //Loop through twice to sort one album for each iteration of the i loop
        for (int i = 0; i < albumsSize; i++)
        {
            for (int j = 0; j < albumsSize - 1; j++)
            {
                /*
                Determine the length of the smallest name out of the two being compared to avoid an index out of bounds
                exception
                 */
                int smallestName = albums[j].getName().length();
                if (smallestName > albums[j + 1].getName().length())
                {
                    smallestName = albums[j + 1].getName().length();
                }

                //Loop through each character of the album names and compare them against each other using ASCII values
                for (int k = 0; k < smallestName; k++)
                {
                    //If the character is the same, ignore it
                    if ((int) albums[j].getName().toLowerCase().charAt(k) == (int) albums[j + 1].getName().toLowerCase().charAt(k))
                    {
                        continue;
                    }
                    /*
                    If the character of the first album comes after the character of the second album alphabetically
                    then they should be swapped
                     */
                    else if ((int) albums[j].getName().toLowerCase().charAt(k) > (int) albums[j + 1].getName().toLowerCase().charAt(k))
                    {
                        tempAlbum = albums[j];
                        albums[j] = albums[j + 1];
                        albums[j + 1] = tempAlbum;
                        //Once a swap has been made there's no need to check the rest of the word
                        break;
                    }
                    else
                    {
                        //If the first album comes before the second album alphabetically, there's no need for a swap
                        break;
                    }
                }
            }
        }

        //Loop through the albums and sort the songs of each album
        for (int i = 0; i < albumsSize; i++)
        {
            albums[i].sortSongs();
        }
    }

    //--------------------- Input/Output methods ---------------------
    public String input(String message)
    {
        /*
         This method shows up a text box with the data in the variable "message" and returns what the user enters in
         the form of a string. It will loop until a valid input has been entered
         */

        //Declare the variable to hold the input the user enters
        String userInput;
        /*
        If the user clicks cancel the result will be null. If the user clicks OK but hasn't typed anything, the result
        will be an empty string. Both of these are invalid, so loop until the user types something and then presses OK
         */
        do
        {
            userInput = JOptionPane.showInputDialog(null, message, "Song Collection", JOptionPane.INFORMATION_MESSAGE);
        }
        while ((userInput == null) || (userInput.length() == 0));
        //Return what the user typed as a String
        return userInput;
    }

    public void output(String message)
    {
        /*
         This method displays the data in the argument "message" for the user, along with an OK button. Programme
         will hang until the OK button has been pressed.
         */

        JOptionPane.showMessageDialog(null, message, "Song Collection", JOptionPane.INFORMATION_MESSAGE);
    }

    public boolean confirm(String message)
    {
        /*
         This method displays the data in the argument "message" for the user, along with a Yes and No buttons. Will
         return true if the user selects yes and false if the user selects no
         */
        if (JOptionPane.showConfirmDialog(null, message, "Song Collection", JOptionPane.YES_NO_OPTION) == 0)
        {
            return true;
        }
        return false;
    }

    //--------------------- Checking methods ---------------------
    public boolean isInteger(String temp)
    {
        /*
         This method takes a string that has been entered by the user, and attempts to parse it into an integer
         variable. If a NumberFormatException is thrown, the variable is not an integer and therefore the function
         returns false. If the parse is successful it returns true
         */

        try
        {
            // Declare a new integer and attempt to parse the argument "temp" into it
            Integer.parseInt(temp);
            // If the above line doesn't throw any errors, return true
            return true;
        }
        catch (NumberFormatException e)
        {
            // If a number format exception is thrown, return false. The argument "temp" is not an integer
            return false;
        }
    }

    public boolean validMenuEntry(String userInput, int lowOption, int highOption)
    {
        /*
         This method takes three arguments. A user input, and then a low option and a high option. Because the menus
         in this programme are displayed numerically and the user is expected to type in a number, the low option is
         the smallest number available on the menu (usually 1), and the high option is the largest number on the menu,
         or the last option that a user could pick. This method first checks that the value the user entered is an
         integer using the isInteger method. After that it checks that it is within the acceptable range, that is
         between the low option and high option. Returns true if it is a valid entry and false if it is invalid.
         */

        if (isInteger(userInput) && (Integer.parseInt(userInput) >= lowOption) && (Integer.parseInt(userInput) <= highOption))
        {
            return true;
        }
        else
        {
            /*
            Either the user has entered something that is not an integer, or they have entered a number that does not
            exist on the menu (less than 1 or more than the number of items in the menu)
             */
            output("Please enter a valid menu item number");
            return false;
        }
    }

    public boolean albumExists(String name)
    {
        /*
        This method takes the name of an album and checks it against all the other albums in the albums array to
        determine if it already exists
         */

        //Cycle through all the albums in a for loop
        for (int i = 0; i < albumsSize; i++)
        {
            //Compare the name of each album with the one passed as the argument
            if (name.toLowerCase().compareTo(albums[i].getName().toLowerCase()) == 0)
            {
                return true;
            }
        }
        return false;
    }

    public boolean shouldAddNewSongWithSameName(String newName)
    {
        /*
        This song takes a name that the user has entered and shows the user a list of all songs in the collection that
        already have that name. Confirm with the user if they still wish to continue
         */

        //The output will be stored in this variable to be presented to the user
        String userOutput;
        /*
        The getSongsOfSameName method takes a name and returns all the songs that share that name. The method is used
        in a number of areas of the program and so has a boolean argument to control the output, a value of true adding
        a message at the top
         */
        userOutput = getSongsOfSameName(newName, true);
        //If there are no songs of the same name, the method will return "-1"
        if (userOutput.compareTo("-1") == 0)
        {
            return true;
        }
        userOutput += "\nDo you wish to continue?";
        /*
        Ask the user to confirm whether they still wish to continue after seeing a list of all the songs of the same
        that currently exist in the collection
         */
        return confirm(userOutput);
    }

    //--------------------- Collection Building ---------------------
    public void makeAlbum(String albumName)
    {
        /*
        This method creates a new album object in the albums array and then increase the logical size of the array
         */
        albums[albumsSize] = new Album(albumName);
        albumsSize++;
    }

    public int makeSong(String songName, String songArtist, String songGenre, int songDuration, Album tempAlbum)
    {
        /*
        This method takes the details of a new song and a reference to an album object, and creates and adds the song
        to that album
         */

        //In order to check if this is a valid song, create a temporary song object
        Song tempSong = new Song(songName, songArtist, songGenre, songDuration);

        //Next check if the song already exists in the album using the Album Class's hasDuplicate method
        if (tempAlbum.hasDuplicate(tempSong))
        {
            /*
            If the song already exists, return an exit code of 1. This method is used when loading text files as well
            as manual entry, and as such should not be showing messages in this method if there are errors. Instead,
            return them as error codes where they can be handled in the manual entry method, or ignored for text files
             */
            return 1;
        }

        //If there is no duplicate, ensure the duration does not exist the maximum playtime for the album
        if (tempAlbum.getTotalTime() + songDuration > tempAlbum.getMaxTime())
        {
            /*
            If the song is going to push the total play time to higher than the maximum playtime, return an exit code
            of 2. This method is used when loading text files as well as manual entry, and as such should not be
            showing messages in this method if there are errors. Instead, return them as error codes where they can be
            handled in the manual entry method, or ignored for text files
             */
            return 2;
        }

        //If the program has reached this point of the method, the song is valid and can be added to the album
        tempAlbum.addSong(tempSong);

        //Return a successful exit code of 0
        return 0;
    }

    //--------------------- Data Obtaining ---------------------
    public int getAlbumSelection(String promptingMessage)
    {
        /*
        This method builds a menu of all albums in the collection and displays these along with a return to main menu
        option to the user. The user is then prompted to select an album or select return to main menu, and then the
        function returns either an index in the albums array or returns -1 if the user requests the main menu
         */

        /*
        This method is called from a number of different places throughout the program. Therefore a prompting message
        is provided that is specific to where this method is called from. Add to the String menuOptions which will keep
        track of the menu that will be displayed to the user
         */
        String menuOptions = promptingMessage + "\n";
        // The menu item that the user selects will be stored here initially
        String menuUserInput;
        // menuUserInput will be converted to an integer and stored here
        int albumSelection;

        //Loop throught he albums array and add each album to the menu options along with some formatting
        for (int i = 0; i < albumsSize; i++)
        {
            menuOptions += (i + 1) + ". " + albums[i].getName() + "\n";
        }

        //Add a return to main menu option to this menu
        menuOptions += (albumsSize + 1) + ". Return to main menu\n";

        //With the menu complete, display to the user and loop until an album is selected
        do
        {
            menuUserInput = input(menuOptions + "What do you wish to do?");
        }
        /*
        Using the validMenuEntry method, check that the user enters an integer number that corresponds the the menu.
        albumsSize is increased by 1 as the user could specify return to main menu
         */
        while (!validMenuEntry(menuUserInput, 1, albumsSize + 1));

        //With a valid input from the user, convert it to an integer
        albumSelection = Integer.parseInt(menuUserInput);

        //Check whether the user wishes to return to main menu
        if (albumSelection == albumsSize + 1)
        {
             /*
            A return value of -1 will tell the method that this was called from that it should simply return the user
            to the main menu
             */
            return -1;
        }
        else
        {
            /*
            If the user has not selected main menu, return the index for the album selected, -1 as the menu starts from
            1 but indexes start from 0
             */
            return albumSelection - 1;
        }
    }

    public int convertDurationToSecs(String userInput)
    {
        /*
        This method will take a duration in the format mm:ss and return an integer of that duration in seconds.
        In the case of an invalid format, the method will return -1.
        */

        /*
        Start by declaring the variables to store the Strings for the Minute portion and the Second portion, and the
        index within the user's input that represents where the colon is
         */
        String minPortion;
        String secPortion;
        int colonIndex;

        /*
        First check that a colon is present in the input the user has provided. If there is no colon, the variable
        colonIndex will be == -1.
         */
        colonIndex = userInput.indexOf(":");

        /*
         * if colonIndex == -1 this means there is no colon and it is therefore is not a valid input
         * Also check that the user has only entered one colon. If the last index is the same as the first index,
         * There is one colon. If they are different, there is at least two colons. This method will return if there is
         * either no colon, or more than one colon. An invalid piece of data is returned as a -1 to differentiate
         * between a valid duration.
         */
        if ((colonIndex == -1) || (userInput.lastIndexOf(":") != colonIndex))
        {
            return -1;
        }

        //There is a single colon present, so now take the first half for "minutes" portion
        minPortion = userInput.substring(0, colonIndex);

        /*
        And take the second half for the "seconds" portion. A substring call with one argument will take from the
        specified index to the end of the string
         */
        secPortion = userInput.substring(colonIndex + 1);

        /*
        Check both portions seperately. If either of them is not an integer, the user has entered invalid data, in
        which case return -1
         */
        if (!isInteger(minPortion) || (!isInteger(secPortion)))
        {
            return -1;
        }

        //Finally check that the second portion is not greater than 59 seconds. If it is, return -1
        if (Integer.parseInt(secPortion) > 59)
        {
            return -1;
        }

        //Finally calculate the duration and return
        return Integer.parseInt(minPortion) * 60 + Integer.parseInt(secPortion);
    }

    public String getGenreSelection(String promptingMessage)
    {
        /*
        This method asks the user to enter a number 1, 2, 3, 4 to pick a genre. It is used in several parts of the
        program and as such has a prompting message that can be specific to where it's called from
         */

        //Declare the variable to store the menu item selected by the user
        String menuInput;

        /*
        This method is called in different parts of the program and so prompting message may be different depending on
        which method has called it. Regardless, the menu itself will be the same as there are only 4 options for genre
         */
        promptingMessage = promptingMessage + "\n1: Rock\n2: Pop\n3: Hip-hop\n4: Bossa nova";

        //Ask the user for the genre
        do
        {
            menuInput = input(promptingMessage);
        }
        //Loop until a valid response has been entered
        while (!validMenuEntry(menuInput, 1, 4));

        //menuInput is currently a menu number so use a switch case to return the correct genre
        switch (menuInput)
        {
            case "1":
            {
                return "Rock";
            }
            case "2":
            {
                return "Pop";
            }
            case "3":
            {
                return "Hip-hop";
            }
            default:
            {
                /*
                By this point it has bene confirmed that the menuInput is valid which means it is either 1, 2, 3, or 4.
                If it is not the other 3 it is safe to assume it must be the last one with a default case
                 */
                return "Bossa nova";
            }
        }
    }

    public String getSongsOfSameName(String newName, boolean showLeadingMessage)
    {
        /*
        This method takes the name of a song and returns a string with all the songs in the collection that have the
        same name as the one specified. It is called from several parts of the program and as such has a boolean
        argument to show a message at the start
         */

        /*
        The songs will be added first to this array, 2 items at a time the first item being the album index and the
        second item being the song index
         */
        int[] songsWithSharedNames = new int[10];
        //Keep track of the logical size of the array
        int songsWithSharedNamesSize = 0;

        //Loop through the albums array and inside that each songs array using two for loops
        for (int i = 0; i < albumsSize; i++)
        {
            for (int j = 0; j < albums[i].getSongsSize(); j++)
            {
                //Check if the song in by the current iteration is the same as the argument passed to this method
                if (albums[i].getSongName(j).toLowerCase().compareTo(newName.toLowerCase()) == 0)
                {
                    //Add the album and the song into songsWithSharedNames array
                    songsWithSharedNames[songsWithSharedNamesSize] = i;
                    songsWithSharedNames[songsWithSharedNamesSize + 1] = j;
                    //Increase the logical size of the array
                    songsWithSharedNamesSize += 2;
                    /*
                    If the logical size of the array has reached the phyisical size, add some more spaces using the
                    resizeIntArray method
                     */
                    if (songsWithSharedNamesSize == songsWithSharedNames.length)
                    {
                        songsWithSharedNames = resizeIntArray(songsWithSharedNames);
                    }
                }
            }
        }

        //If the logical size of the array is 0, no songs exist that share a name with that specified
        if (songsWithSharedNamesSize == 0)
        {
            return "-1";
        }

        /*
        If there is 1 song that shares a name, compared to any other number of songs, it will require plurals in
        different parts of the message
         */
        String s1 = "s";
        String s2 = "";
        if (songsWithSharedNamesSize == 2)
        {
            s1 = "";
            s2 = "s";
        }

        String userOutput = "";
        //Only add this message if the argument is true
        if (showLeadingMessage)
        {
            userOutput += "The following song" + s1 + " already exist" + s2 + " in this collection: \n\n";
        }
        //Loop through the songsWithSharedNamesSize array 2 at a time
        for (int i = 0; i < songsWithSharedNamesSize; i += 2)
        {
            //For formatting purposes check if this is the first song in the array or not
            if ((i > 0) && (songsWithSharedNames[i] == songsWithSharedNames[i - 2]))
            {
                userOutput += "    Name: ";
            }
            else
            {
                userOutput += albums[songsWithSharedNames[i]].getName() + "\n    Name: ";
            }
            /*
            The getSongDetails method is used in a number of areas of the program and as such requires two boolean
            arguments to control the kind of output received. "true" in the second argument will return all details of
            the songs and "false" will only return the song names. The value of the third argument purely changes the
            formatting, "true" will have no spaces and is used when saving text files. For most methods it will be
            "false" and will have spaces and indents.
             */
            userOutput += albums[songsWithSharedNames[i]].getSongDetails(songsWithSharedNames[i + 1], true, false);
        }
        //Return the String to the original method
        return userOutput;
    }

    public int[] getSongToDelete()
    {
        /*
        This Method gathers a list of all the songs that have been created so far and puts them into alphabetical order.
        It will then display a menu to the user and ask them to select a song, or return to the main menu. If the user
        selects a song, this method will return an array of length 2. The first item refers to the index in the albums
        array, while the second item refers to the index in the songs array within that album. For the rest of this
        method's documentation, this String will be referred to as the Album Song Code. If the user selects to return
        to main menu, this method returns "-1".

        As the songs are searched and ordered into alphabetical order, they will be added into this reference. They
        will added in the format of Album Song Codes. As a result this reference will be a record of where each of the
        songs come from, except ordered in alphabetical order.
         */
        int[][] songsInMenuReference;
        //Keep track of the logical size of the array
        int songsInMenuReferenceSize = 0;
        /*
        The algorithm to order the songs into alphabetical order makes use of the ASCII value of the characters. This
        variable will keep track of the lowest ASCII value it has found so far. The lowest value at the end is the one
        that comes first alphabetically
         */
        int recordBest;
        /*
        In addition to keeping track of the best ASCII code found, the Album Song Code for that song needs to also be
        recorded so that it can be added to the songsInMenuReference at the end
         */
        int[] recordBestCode;

        //This String will hold the actual menu options for when the user is finally asked to enter a song to delete
        String menu;
        //Store the input from the user when they make a selection
        String userInput;
        //The user input will be converted to an integer to perform some mathematical operations
        int userInputInt;

        /*
        The songsInMenuReference array and the menu String will be built simultaneously. This algorithm works by
        looping once for every song in the collection, and then for each song, compares it against every other song in
        the collection to figure out the next song in alphabetical order. Each time the i loop runs a new song will be
        added to the songsInMenuReference.

        Start with a prompting message
        */
        menu = "What song do you wish to delete?\n\n";

        /*
        Loop through the albums array and for each album add the songsSize to totalSongs to figure out how many songs
        exist in the collection
         */
        int totalSongs = 0;
        for (int i = 0; i < albumsSize; i++)
        {
            totalSongs += albums[i].getSongsSize();
        }

        //Initialise the songsInMenuReference array with the exact number of songs in the collection
        songsInMenuReference = new int[totalSongs][2];

        for (int i = 0; i < totalSongs; i++)
        {
            /*
            Declare a dummy recordBest. An ASCII > 255 cannot be typed on a keyboard so there's no way the user has
            entered this as a song name. The first song the user has entered will immediately beat this score, and run
            from there.
             */
            recordBest = 255;
            //The best Album Song Code found so far will be stored here and updated if a new best if found
            recordBestCode = new int[2];

            //As described above, loop through the albums and songs with nested for loops to compare against each song
            for (int j = 0; j < albumsSize; j++)
            {
                for (int k = 0; k < albums[j].getSongsSize(); k++)
                {
                    /*
                    If the first character of the song in question is lower than the ASCII value in recordBest, and
                    the song is not already in songsInMenuReference, it will become the new candidate for the next song
                    to be added
                     */
                    if (((int) albums[j].getSongName(k).toLowerCase().charAt(0) < recordBest) && (!isSongInMenuReference(j, k, songsInMenuReference)))
                    {
                        //Update the record best to the song in question
                        recordBest = albums[j].getSongName(k).toLowerCase().charAt(0);
                        //j in this case is the album index and k is the song index
                        recordBestCode[0] = j;
                        recordBestCode[1] = k;
                    }
                }
            }

            /*
            After the j loop has been completed, the recordBestCode should hold the Album Song Code of the song that
            comes earliest in the alphabet, and has not already been added to the songsInMenuReference. As a result
            add it now.
             */

            songsInMenuReference[songsInMenuReferenceSize][0] = recordBestCode[0];
            songsInMenuReference[songsInMenuReferenceSize][1] = recordBestCode[1];
            //Increase the logical size of the array
            songsInMenuReferenceSize++;

            //Add the song to the menu String, using the logical size of the array as the menu item number
            menu += songsInMenuReferenceSize + ". ";
            menu += albums[recordBestCode[0]].getSongName(recordBestCode[1]) + " (" + albums[recordBestCode[0]].getName() + ")\n";
        }
        /*
        If after all the for loops the logical size of the songsInMenuReference array is 0, there mustn't have been any
        songs to add, that is they haven't been created yet. A -1 in the first item of the array returned indicates an
        error
         */
        if (songsInMenuReferenceSize == 0)

        {
            output("There are no songs in this collection.");
            int[] temp = {-1, 0};
            return temp;
        }

        //Add a return to main menu item to the menu
        menu += (songsInMenuReferenceSize + 1) + ". Return to main menu";

        //With the menu completed, show it to the user and request an input
        do
        {
            userInput = input(menu);
        }
        //If the user does not enter an integer number that is a valid option on the menu, loop until they do
        while (!validMenuEntry(userInput, 1, songsInMenuReferenceSize + 1));

        //If the user has chosen the menu option "return to main menu", Return a -1 in the first item of the array
        if (Integer.parseInt(userInput) == songsInMenuReferenceSize + 1)
        {
            int[] temp = {-1, 0};
            return temp;
        }

        //Convert the user entered menu option to an integer and subtract 1 so that it can be used as an index
        userInputInt = Integer.parseInt(userInput) - 1;

        //Finally, return the Album Song Code for the song that the user wishes to delete.
        return songsInMenuReference[userInputInt];
    }

    public boolean isSongInMenuReference(int albumNumber, int songNumber, int[][] reference)
    {
        /*
        This method takes an album and song index and determines if those values are in the reference list provided
         */

        //Loop through the reference list
        for (int i = 0; i < reference.length; i++)
        {
            //Check that both the album and song indexes are a perfect match, if there is any match found, return true
            if ((albumNumber == reference[i][0]) && (songNumber == reference[i][1]))
            {
                return true;
            }
        }
        //If no matches have been found, return false
        return false;
    }

    //--------------------- Text Files ---------------------
    public void readFromTextFile()
    {
        /*
        This method presents a list of all text files in the source directory and requests the user to select one, or
        select to start with an empty collection. If the user selects to load from a text file, that file will be
        opened and the information ported into the collection
         */

        //Get the current path of the source directory
        Path dir = Paths.get(System.getProperty("user.dir"));
        //Start the menu with a prompting message
        String menuOutput = "What file do you wish to load from?\n";
        //The menu item the user picks will be stored here
        String userInput;
        //Keep track of all text file names in the source directory
        String[] files = new String[5];
        //Logical size of the array
        int numberOfFiles = 0;
        //As each file is added to the menu it will be temporarily stored here
        String tempFilename;

        //Start by finding all text files in the source directory by opening a DirectoryStream
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir))
        {
            //Loop through each file found in the DirectoryStream
            for (Path entry : stream)
            {
                //If the file name is a text file, it will end in the extension .txt
                if (String.valueOf(entry.getFileName()).endsWith(".txt"))
                {
                    //Save the file name and add it to the files array
                    tempFilename = String.valueOf(entry.getFileName());
                    files[numberOfFiles] = tempFilename;
                    /*
                    Add it to the menu by takeing the substring of the first part of the name in order to remove the
                    .txt from the end
                     */
                    menuOutput += (numberOfFiles + 1) + ". " + tempFilename.substring(0, tempFilename.length() - 4) + "\n";
                    //Increase the logical size of the array
                    numberOfFiles++;
                    /*
                    If the logical size of the array is the same as the physical size of the array, increase the
                    physical size using the resizeStringArray method
                     */
                    if (numberOfFiles == files.length)
                    {
                        files = resizeStringArray(files);
                    }
                }
            }
        }
        catch (IOException e)
        {
            //If there is an error loading any of the files, just return which will initialise with an empty collection
            return;
        }

        //If there are no text files in the source directory, just return which will initialise with an empty collection
        if (numberOfFiles == 0)
        {
            return;
        }
        //Add an option to initialise with an empty collection
        numberOfFiles++;
        menuOutput += numberOfFiles + ". No file - start with an empty collection";

        //Present the menu to the user and ask them to select an option
        do
        {
            userInput = input(menuOutput);
        }
        //Loop until the user enters a valid integer that is an option on the menu
        while (!validMenuEntry(userInput, 1, numberOfFiles));

        //If the user selects no file load, simply return which will simply initialise with an empty collection
        if (Integer.parseInt(userInput) == numberOfFiles)
        {
            return;
        }

        /*
        Save the filename by comparing the user input to the files array, and save this name in the global
        collectionName variable
         */
        String filename = files[Integer.parseInt(userInput) - 1];
        collectionName = filename;

        //Declare a Scanner to read the text file
        Scanner inputStream;
        try
        {
            //Try opening the Scanner with the filename selected
            inputStream = new Scanner(new File(filename));
        }
        catch (FileNotFoundException e)
        {
            //If there is an error opening the Scanner, return which will initiallise with an empty collection
            return;
        }

        //Each line of text will be stored here as it is read so that it can be interpretted
        String textLine;

        //Variables to hold the album info
        String currentAlbumName = "";
        Album currentAlbum = new Album("");

        //Variables to hold the song info
        String songName = "";
        String artist = "";
        int duration = 0;
        String genre = "";

        //This array will ensure that all information has been read about a song before it is added to the collection
        boolean[] dataCounter = {false, false, false, false};
        //Loop while there is still information to read from the text file
        while (inputStream.hasNext())
        {
            //Read the text file line by line
            textLine = inputStream.nextLine();
            //If the line starts with the word "Album", create a new album with the name being whatever follows "Album"
            if (textLine.startsWith("Album"))
            {
                currentAlbumName = textLine.substring(6);
                /*
                If the album has already been created (Say the collection is out of order and there are some songs from
                one album and then from another and then some from the first again), only create the album once. Also
                ensure that there is space in the albums array before creating the album
                 */
                if ((!albumExists(currentAlbumName)) && (albumsSize != albums.length))
                {
                    makeAlbum(currentAlbumName);
                }
            }
            //If the line starts with the word "Name", assume it's the name of a song in the current album
            else if (textLine.startsWith("Name"))
            {
                songName = textLine.substring(5);
                dataCounter[0] = true;
            }
            //If the line starts with the word "Artist", assume it's the Artist of a song in the current album
            else if (textLine.startsWith("Artist"))
            {
                artist = textLine.substring(7);
                dataCounter[1] = true;
            }
            //If the line starts with the word "Duration", assume it's the Duration of a song in the current album
            else if (textLine.startsWith("Duration"))
            {
                String temp = textLine.substring(9);

                //First assume it's in the format mm:ss
                duration = convertDurationToSecs(temp);
                if (duration == -1)
                {
                    //If duration is now -1, the duration must have been in the format of an integer number of seconds
                    if (isInteger(temp))
                    {
                        //In this case parse it straight in
                        duration = Integer.parseInt(temp);
                    }
                }
                if (duration != -1)
                {
                    dataCounter[2] = true;
                }
            }
            //If the line starts with the word "Genre", assume it's the Genre of a song in the current album
            else if (textLine.startsWith("Genre"))
            {

                genre = textLine.substring(6);
                String[] genres = {"Rock", "Pop", "Hip-hop", "Bossa nova"};
                //Loop through to determine which genre it is referring to, and then add it in
                for (int i = 0; i < 4; i++)
                {
                    if (genre.toLowerCase().compareTo(genres[i].toLowerCase()) == 0)
                    {
                        genre = genres[i];
                        dataCounter[3] = true;
                    }
                }
            }

            //If all 4 items in the dataCounter array are true, the complete information for a song has been read
            if ((dataCounter[0]) && (dataCounter[1]) && (dataCounter[2]) && (dataCounter[3]))
            {
                //Loop through the albums and find the name of the current album that is being read
                for (int i = 0; i < albumsSize; i++)
                {
                    if (albums[i].getName().compareTo(currentAlbumName) == 0)
                    {
                        //Save the album that is currently being added to in the currentAlbum variable
                        currentAlbum = albums[i];
                    }
                }

                /*
                Ensure that the album is not full, and if not, add the song to the album using the makeSong method.
                Ignore any error codes. If there is an error such as the song already existing or going for too long,
                the song will simply not be added to the collection
                 */
                if (!currentAlbum.isFull())
                {
                    makeSong(songName, artist, genre, duration, currentAlbum);
                }
                //Reset the dataCounter array ready for a new song
                dataCounter[0] = false;
                dataCounter[1] = false;
                dataCounter[2] = false;
                dataCounter[3] = false;
            }
        }
        //After the text file has been read, close the Scanner and show a success message to the user
        inputStream.close();
        output("Song collection successfully loaded!");
    }

    public boolean saveTextFile()
    {
        /*
        This method saves the collection to a text file. If the collection was initially loaded from a text file, it
        will be overwritten with the same name. If it was a new collection, the user will specify a name
         */

        /*
        The collectionName variable is initiallised as an empty String "" so if it hasn't been overwritten, ask the
        user to specify a new name for it, and add .txt at the end
         */
        if (collectionName.length() == 0)
        {
            collectionName = input("What do you want to save this collection as?") + ".txt";
        }
        //Declare a PrintWriter to create the file
        PrintWriter outputStream;
        try
        {
            //Try to create a PrintWriter with the name specified
            outputStream = new PrintWriter(collectionName);
        }
        catch (FileNotFoundException e)
        {
            //If there is an error, return false to the original method to indicate a failed save
            return false;
        }

        //If the PrintWriter has been opened successfully, loop through the albums to start saving information
        for (int i = 0; i < albumsSize; i++)
        {
            //First add the name of the album
            outputStream.println("Album " + albums[i].getName());
            //Loop through all the songs and add the details of each song
            for (int j = 0; j < albums[i].getSongsSize(); j++)
            {
                /*
                The showSongs method is used in a number of areas of the program and as such requires two boolean
                arguments to control the kind of output received. "true" in the first argument will return all details
                of the songs and "false" will only return the song names. The value of the second argument purely
                changes the formatting, "true" will have no spaces and is used here. For most methods it will be "false"
                and will have spaces and indents.
                */
                outputStream.println(albums[i].getSongDetails(j, true, true));
            }
        }
        //Once the albums have been looped through, close the PrintWriter and return true as a successful save
        outputStream.close();
        return true;
    }

    //--------------------- Array Resizing ---------------------

    public String[] resizeStringArray(String[] oldArray)
    {
        /*
        This method takes a String array and adds 5 new spots in it
         */

        //Keep track of the length of the old array
        int length = oldArray.length;
        //Declare a new array with 5 extra spaces
        String[] newArray = new String[length + 5];
        //Loop through the old array, and add each item in the array into the new array
        for (int i = 0; i < length; i++)
        {
            newArray[i] = oldArray[i];
        }
        //Return the new array
        return newArray;
    }

    public int[] resizeIntArray(int[] oldArray)
    {
        /*
        This method takes an integer array and adds 10 new spots in it
         */

        //Keep track of the length of the old array
        int length = oldArray.length;
        //Declare a new array with 10 extra spaces
        int[] newArray = new int[length + 10];
        //Loop through the old array, and add each item in the array into the new array
        for (int i = 0; i < length; i++)
        {
            newArray[i] = oldArray[i];
        }
        //Return the new array
        return newArray;
    }
}