Album [title of show]
Name A Way Back To Then
Artist Jeff Bowen
Genre Rock
Duration 2:33


Name I Am Playing Me
Artist Jeff Bowen
Genre Rock
Duration 1:41


Name The Tony Award Song
Artist Jeff Bowen
Genre Rock
Duration 0:26


Name Two Nobodies In New York
Artist Jeff Bowen
Genre Rock
Duration 2:51


Name Untitled Opening Number
Artist Jeff Bowen
Genre Rock
Duration 2:15


Album Pippin
Name Corner Of The Sky
Artist Stephen Schwartz
Genre Rock
Duration 3:17


Name Kind Of Woman
Artist Stephen Schwartz
Genre Rock
Duration 2:13


Name On The Right Track
Artist Stephen Schwartz
Genre Rock
Duration 3:33


Name Simple Joys
Artist Stephen Schwartz
Genre Rock
Duration 2:56


Album Rent
Name One Song Glory
Artist Jonathon Larson
Genre Rock
Duration 2:46


Name Rent
Artist Jonathon Larson
Genre Rock
Duration 3:58


Name Seasons of Love
Artist Jonathon Larson
Genre Rock
Duration 3:02


Name You'll See
Artist Jonathon Larson
Genre Rock
Duration 2:14


Album Shrek the Musical
Name Build A Wall
Artist David Lindsay-Abaire
Genre Rock
Duration 2:40


Name Story Of My Life
Artist David Lindsay-Abaire
Genre Rock
Duration 3:10


Name This Is Our Story
Artist David Lindsay-Abaire
Genre Rock
Duration 3:16


Name Travel Song
Artist David Lindsay-Abaire
Genre Rock
Duration 1:56


